class Donation < ActiveRecord::Base

  belongs_to :donor

  def self.totalamount
    @donationtotals = Donation.sum(:amount)
  end

  def self.donationcount
    @donationcount = Donation.count
  end

  def self.personaltotal
    @personaltotal = Donation.sum(:amount)
  end

end
