json.array!(@donations) do |donation|
  json.extract! donation, :id, :date, :amount, :donor_id
  json.url donation_url(donation, format: :json)
end
